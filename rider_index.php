<?php 
    session_start();
    include('server_shop.php');
    
    if(!isset($_SESSION['username'])){
        $alert = '<script type="text/javascript">';
        $alert .= 'alert("ต้อง login ก่อน!");';
        $alert .= 'window.location.href ="login.php";';
        $alert .= '</script>';
      echo $alert;
    }

    if(isset($_SESSION['address'])){
        $alert = '<script type="text/javascript">';
        $alert .= 'alert("ต้องมี Adderssก่อน");';
        $alert .= 'window.location.href ="login.php";';
        $alert .= '</script>';
        echo $alert;
    }

    if(isset($_GET['logout'])){
        $alert = '<script type="text/javascript">';
        $alert .= 'alert("ขอบคุณที่ใช้บริการค่ะ");';
        session_destroy();
        unset($_SESSION['username']);
        $alert .= 'window.location.href ="login.php";';
        $alert .= '</script>';
        echo $alert;
    }
    $all=0;
    $username = $_SESSION['username'];

    $mysql = "SELECT * FROM rider WHERE username='$username'";
    $query_user= mysqli_query($conn,$mysql);
    $user = mysqli_fetch_assoc($query_user);

    $mysql = "SELECT * FROM invoice WHERE status ='กำลังหาไรเดอร์'";
    $query_order= mysqli_query($conn,$mysql);
    $list_order = mysqli_fetch_assoc($query_order);

    $mysql = "SELECT * FROM invoice WHERE status ='กำลังทำอาหาร'";
    $query = mysqli_query($conn,$mysql);
    $received_order = mysqli_fetch_assoc($query);
    
    //$shopid = $result['shopid'];

    //$mysql = "SELECT * FROM Menu WHERE shopid = '$shopid'";
    //$query_show = mysqli_query($conn,$mysql);

    //debug
    //foreach($query_user as $data):
    //    echo '<pre>';
    //    print_r($data);
    //    echo '</pre>';
   // endforeach;

   // foreach($query_show as $data):
    //echo '<pre>';
    //print_r($data);
    //echo '</pre>';
   // endforeach;

    //debug
    //echo '<pre>';
    //print($id);
    //print_r($result);
    //echo '</pre>';

 ?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<script type="text/javascript" src="https://api.longdo.com/map/?key=93a8dc22b5f210686e3140ad2ce40482"></script>
    <title>Welcome</title>
    
</head>

<style>
     button{
        border-radius: 0.5em;
        font-size:13px;
     }
     body{
        font-family: Tahoma, sans-serif;
        background: #ffffff;
     }
     .bar{
        width:100%;
        height:9%;
        position:absolute;
        background-color: #DC143C;
        display: inline-block;
     }
     .dropbtn{
        background: url('picture/menu.png');
        cursor: pointer;
        margin-left:50px;
        margin-top:20px;
        padding: 25px;
        border:none;
     }
     .dropdown {
        position: relative;
        display: inline-block;
    }
    .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f1f1f1;
        min-width: 160px;
    }
    .dropdown-content a {
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
    }
    .dropdown:hover .dropdown-content {display: block;}

    .dropdown-content a:hover {background-color: #ddd;}

    .search-bar{
        margin-left:500px;
        margin-top:-60px;
    }
    .search{
        background: url('picture/search.png');
        margin-top:10px;
        margin-left:10px;
        border:none;
        height: 20px;
        width: 20px;
    }
     .picture-group{
         border-radius: 25m; 
         margin-left: 170px;
     }
     .tab{
        width:39%;
        height:0.25%;
        position:absolute;
        background-color: #000000;
     }
     .detail-group{
         width:750px;
         height:auto;
         border-radius: 2em;
         background: #FFFFFF; 
         margin-left: 170px;
     }
     .menu-group{
         width:auto;
         height: 732px;
         border-radius: 2em;
         background: #FFFFFF; 
         right:0;
     }
     .group-image{
         border-radius: 2em;
         background: #CDCDCD;
         width: 200px;
         height: 150px;
         text-align: center;
         padding: 10px;
     }
     a:link, a:visited {
        color: #000000;
        text-decoration: underline;
        cursor: pointer;
    }
    .avatar{
        border-color: #000000;
        border-radius: 2em;
    }
    .frame {
        width: 300px;
        height: 250px;
        border: 3px solid #ccc;
        background: #eee;
        margin: auto;
    }


    
</style>
<body>
    <div class="header">
    <title>Eater</title>
    </div>
    

            <!-- บาร์ข้างบน -->
            <div class="bar">
        <a href="rider_index.php">
        <img src="picture\logo.png" alt="logo" width="100" style="margin-left:30px;float:left;">
        </a>
        
            <!-- MENU -->
            <div class="dropdown">
                <button class="dropbtn"></button>
                <div class="dropdown-content">
                    <a href="profile_rider.php?<?php echo $user['riderid'];?>">Profile</a>
                    <a href="?logout='1'">Logout</a>
                </div>
            </div>

            
        </div>
            <br><br><br><br>
        
            <!-- content -->
            <form action="rider_received.php" method="POST" enctype="/form-data" >
            <div class="container p-2 bg-white rounded position-absolute w-50 top-50 start-0 translate-middle-y border border-secondary" style="max-width:40%; margin-left:150px;margin-top:-100px;">
            <h1 class="p-3">Order available</h1>
            <hr>
            <?php foreach($query_order as $data): ?>
                <div class="row ">
                    <div class="col">
                    <input type="radio" name="order" value="<?=$data['orderid']?>" class="form-check-input">
                    <label ><?php echo $data['shopname']; ?></label>
                    </div>

                    <div class="col">
                    <label ><?php echo $data['foodname']; ?></label>
                    </div>

                    <div class="col">
                    <label ><?php echo $data['totalprice']; ?> ฿</label>
                    </div>
                </div> <!-- row -->
                <hr>
        <?php endforeach; ?>

        </div> <!-- container -->
<br>

            <div class="col">
            <div class="container p-5 bg-white rounded position-absolute top-50 end-0 translate-middle-y border border-secondary" style="max-width:30%; margin-right:200px;margin-top:-150px;">
                <h1>Order received</h1>
                <hr>
                <?php foreach($query as $data): ?>
                <div class="row ">
                    <div class="col" >
                    <label ><?php echo $data['shopname']; ?></label>
                    </div>

                    <div class="col">
                    <label ><?php echo $data['foodname']; ?></label>
                    </div>

                    <div class="col">
                    <label ><?php echo $data['totalprice']; ?> ฿</label>
                    <a class="btn btn-dark text-white" href="order_detail.php?<?=$data['orderid'];?>">ตรวจสอบ</a>
                    </div>
                
                </div> <!-- row -->
                <hr>
        <?php endforeach; ?>
            
            </div> <!-- container -->
            <input type="hidden" id="riderid" name="riderid" value="<?=$list_order['riderid'];?>">
            <button type="submit" class="btn btn-success w-50 p-3 fs-3 position-absolute bottom-0 end-0" style="max-width:30%; margin-right:200px;margin-bottom:300px;" name="palm" href="order_detail.php?<?=$list_order['orderid'];?>">รับออร์เดอร์</button>
            
            </form>
</body>
</html>