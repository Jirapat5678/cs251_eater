<?php 
    session_start();
    include('server.php');
    
    if(!isset($_SESSION['username'])){
      $alert = '<script type="text/javascript">';
      $alert .= 'alert("ต้อง login ก่อน!");';
      $alert .= 'window.location.href ="login.php";';
      $alert .= '</script>';
      echo $alert;
    }

    if(isset($_SESSION['address'])){
        $alert = '<script type="text/javascript">';
        $alert .= 'alert("ต้องมี Adderssก่อน");';
        $alert .= 'window.location.href ="login.php";';
        $alert .= '</script>';
        echo $alert;
    }

    if(isset($_GET['logout'])){
        $alert = '<script type="text/javascript">';
        $alert .= 'alert("ขอบคุณที่ใช้บริการค่ะ");';
        session_destroy();
        unset($_SESSION['username']);
        $alert .= 'window.location.href ="login.php";';
        $alert .= '</script>';
        echo $alert;
        
    }

    $username = $_SESSION['username'];


    $mysql = "SELECT shopid,shopname, img_dir FROM Shoper";
    $query_shop = mysqli_query($conn,$mysql);

    $mysql = "SELECT * FROM rider WHERE username = '$username'";
    $query_user = mysqli_query($conn,$mysql);
    $user = mysqli_fetch_assoc($query_user);

    //debug
    //foreach($query_user as $data):
     //   echo '<pre>';
      //  print_r($data);
       // echo '</pre>';
    //endforeach;

    //foreach($query_shop as $data):
    //echo '<pre>';
    //print_r($data);
    //echo '</pre>';
    //endforeach;
 ?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<script type="text/javascript" src="https://api.longdo.com/map/?key=93a8dc22b5f210686e3140ad2ce40482"></script>
    <title>Eater</title>
    
</head>
<style>
     input{
        border-radius: 0.5em;
        border-width: 0;
        box-shadow: 0 0 0 0.7pt #000000;
        height: 2em;
        width: 17em;
        font-size: 16px;
     }
     body{
        font-family: Tahoma, sans-serif;
        background: linear-gradient(
          rgba(0, 0, 0, 0.4), 
          rgba(0, 0, 0, 0.4)
        ),
        url('picture/BG.jpg');
     }
     .input-group{
         width: 350px;
         padding: 50px;
         border-radius: 2em;
         position: absolute;
         top: 25%;
         left: 39%;
         background: #E6E5E5;
         text-align: center;
     }
     .bar{
        width:100%;
        height:9%;
        position:absolute;
        background-color: #DC143C;
        display: inline-block;
     }
     .tab{
        width:76.50%;
        height:0.25%;
        position:absolute;
        background-color: #000000;
     }
     .dropbtn{
        background: url('picture/menu.png');
        cursor: pointer;
        margin-left:50px;
        margin-top:20px;
        padding: 25px;
        border:none;
     }
     .dropdown {
        position: relative;
        display: inline-block;
    }
    .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f1f1f1;
        min-width: 160px;
    }
    .dropdown-content a {
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
    }
    .dropdown:hover .dropdown-content {display: block;}

    .dropdown-content a:hover {background-color: #ddd;}

    .search-bar{
        margin-left:500px;
        margin-top:-60px;
    }
    .search{
        background: url('picture/search.png');
        margin-top:10px;
        margin-left:10px;
        border:none;
        height: 20px;
        width: 20px;
    }
    .suggest{
      padding-left: 90px;
      font-size: 26px;
     }
     .group-suggest{
         width: 1350px;
         height: 500px;
         border-radius: 2em;
         background: #FFFFFF; 
         padding: 10px;
     }
     .group-promotion{
         width: 1350px;
         height: 300px;
         border-radius: 2em;
         background: #FFFFFF; 
         padding: 10px;
     }
     .group-total{
         width: 1350px;
         height: auto;
         border-radius: 2em;
         background: #FFFFFF; 
         padding: 10px;
         margin-left: 210px;
     }
     .image{
      padding-left: 90px;
      margin-left: 50px;
      letter-spacing: 64px;

     }
     .group-image{
         border-radius: 2em;
         background: #CDCDCD;
         width: 200px;
         height: 150px;
         text-align: center;
         padding: 10px;
     }
     .name{
      font-size: 22px;
      padding-left: 160px;
      
     }
     .group-name{
      width: 100px;
         height: 30px;
         border-radius: 2em;
         background: #FFFFFF; 
         padding: 10px;
         margin-left: 75px;
     }
     a:link, a:visited {
        color: #000000;
        text-decoration: underline;
        cursor: pointer;
    }
</style>
<!--
<script>

function init() {
            map = new longdo.Map({
              placeholder: document.getElementById('map_canvas')
            });
          }

          
</script>
        -->
<body>

    <!--
    <div id="map_canvas" style="width:80%;height:100vh"></div>
		</div>
    -->

    
        <!-- อยู่ในระบบแล้ว -->

            <!-- บาร์ข้างบน -->
            <div class="bar">
        <a href="rider_index.php">
        <img src="picture\logo.png" alt="logo" width="100" style="margin-left:30px;float:left;">
        </a>
        
            <!-- MENU -->
            <div class="dropdown">
                <button class="dropbtn"></button>
                <div class="dropdown-content">
                    <a href="profile_rider.php?<?php echo $user['riderid'];?>">Profile</a>
                    <a href="?logout='1'">Logout</a>
                </div>
            </div>

            
        </div>
            <br><br><br><br>

            
        <!--แนะนำร้านอาหาร -->
        <br>
          
       <div class="container p-3 mt-5 bg-white rounded">
           <div class="title">
         <h1 class="fs-3 text-start ms-5"><b>ข้อมูลส่วนตัว</b></h1>
        <hr style="height:2px;color:black;background-color:black;">
        </div>
        <form method="post" enctype="/form-data" action="login_db.php">
        <div class="row p-3 mx-auto">
          <div class="col text-end">
            <label for="username" >Username :</label><br><br>
            <label for="fname" class="mt-2">Name :</label><br><br>
            <label for="lname" class="mt-2">Surname :</label><br><br>
            <label for="tel" class="mt-2" >Phone number :</label><br><br>
            <label for="email" class="mt-2" >E-mail :</label><br><br>
            <label for="address" class="mt-2" >Address :</label><br><br>
            <label for="gen" class="mt-2" >National ID :</label>
        </div>

        <div class="col me-5">
        <input type="text" name="username" value="<?php echo $user['username']; ?>" disabled><br><br>
        <input type="text" name="fname" value="<?php echo $user['firstname']; ?>" ><br><br>
        <input type="text" name="lname" value="<?php echo $user['lastname']; ?>"><br><br>
        <input type="number" name="tel" value="<?php echo $user['tel']; ?>"><br><br>
        <input type="email" name="email" value="<?php echo $user['email']; ?>"><br><br>
        <input type="text" name="address" value="<?php echo $user['address']; ?>"><br><br>
        <input type="number" name="nationalid" value="<?php echo $user['nationalid']; ?>" disabled><br><br>
        <input type="hidden" name="password" value="<?php echo $user['password'] ?>">
        <input type="hidden" name="username" value="<?php echo $user['username'] ?>">
        </div>
        
         

         <button class="btn btn-success" name="update_rider">ยืนยันการเปลี่ยนแปลงข้อมูล</button>
     </div>
        </div>
        </form>
     <!-- รายการโปรโมชั่น -->


            
</body>
</html>