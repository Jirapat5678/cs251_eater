<?php 
    session_start();
    include('server.php');

    $errors = array();

    if(isset($_POST['reg_user'])){
        $username = mysqli_real_escape_string($conn, $_POST['username']);
        $email= mysqli_real_escape_string($conn, $_POST['email']);
        $password_1 = mysqli_real_escape_string($conn, $_POST['password_1']);
        $password_2 = mysqli_real_escape_string($conn, $_POST['password_2']);
        $tel = mysqli_real_escape_string($conn, $_POST['tel']);
        $fname = mysqli_real_escape_string($conn, $_POST['fname']);
        $lname = mysqli_real_escape_string($conn, $_POST['lname']);
        $address = mysqli_real_escape_string($conn, $_POST['address']);

        if($password_1 != $password_2){
            array_push($errors, "The two passwords do not match");
        }

        $user_check_query = "SELECT * FROM Member WHERE username = '$username' OR email = '$email' ";
        $query = mysqli_query($conn, $user_check_query);
        $result = mysqli_fetch_assoc($query);

        if($result){
            if($result['username'] === $username){
                array_push($errors, "Username already exists");
            }
            if($result['email'] === $email){
                array_push($errors, "Email already exists");
            }
        }

        if(count($errors)==0){
            $password =md5($password_1);

            $sql = "INSERT INTO Member (username, email, password, tel, firstname, lastname, address) VALUES ('$username', '$email', '$password', '$tel', '$fname', '$lname', '$address')";
            mysqli_query($conn,$sql);

            $_SESSION['username'] = $username;
            $_SESSION['success'] = "You are now logged in";
            header('location: index.php');
        } else {
            array_push($errors, "Username or Email already exists");
            $_SESSION['error'] = "Username or Email already exists";
            header("location: register.php");
        }
            
    }

    if(isset($_POST['reg_shop'])){
        $dir = "picture/";
        $username = mysqli_real_escape_string($conn, $_POST['username']);
        $email = mysqli_real_escape_string($conn, $_POST['email']);
        $shopname = mysqli_real_escape_string($conn,$_POST['shopname']);
        $available = mysqli_real_escape_string($conn,$_POST['available']);
        $open = mysqli_real_escape_string($conn,$_POST['open']);
        $close = mysqli_real_escape_string($conn,$_POST['close']);
        $password_1 = mysqli_real_escape_string($conn, $_POST['password_1']);
        $password_2 = mysqli_real_escape_string($conn, $_POST['password_2']);
        $tel = mysqli_real_escape_string($conn, $_POST['tel']);
        $address = mysqli_real_escape_string($conn, $_POST['address']); 
        $fileImage = $dir . basename($_FILES["file"]["name"]);

        move_uploaded_file($_FILES["file"]["tmp_name"], $fileImage);

        if($password_1 != $password_2){
            array_push($errors, "The two passwords do not match");
        }


        $user_check_query = "SELECT * FROM Shoper WHERE username = '$username' OR email = '$email' OR shopname ='$shopname' OR img_dir ='$fileImage'";
        $query = mysqli_query($conn, $user_check_query);
        $result = mysqli_fetch_assoc($query);

        if($result){
            if($result['username'] === $username){
                array_push($errors, "Username already exists");
            }
            if($result['email'] === $email){
                array_push($errors, "Email already exists");
            }
            if($result['shopname'] === $shopname){
                array_push($errors, "Shop's name already exists");
            }
            if($result['img_dir'] === $fileImage){
                array_push($errors, "Image's name already exists");
            }
        }

        if(count($errors)==0){
            $password =md5($password_1);

            $sql = "INSERT INTO Shoper (username, email, password, tel, shopname, available, open, close, img_dir, address) VALUES ('$username', '$email', '$password', '$tel', '$shopname', '$available', '$open', '$close', '$fileImage', '$address')";
            mysqli_query($conn,$sql);

            $_SESSION['username'] = $username;
            $_SESSION['success'] = "You are now logged in";
            header('location: shoper_index.php');
        } else {
            array_push($errors, "Username or Email already exists");
            $_SESSION['error'] = "Username or Email already exists";
            header("location: shop_register.php");
        }
            
    }

    if(isset($_POST['reg_rider'])){
       
        $username = mysqli_real_escape_string($conn, $_POST['username']);
        $firstname = mysqli_real_escape_string($conn, $_POST['firstname']);
        $lastname = mysqli_real_escape_string($conn, $_POST['lastname']);
        $nationalid = mysqli_real_escape_string($conn, $_POST['nationalid']);
        $email= mysqli_real_escape_string($conn, $_POST['email']);
        $password_1 = mysqli_real_escape_string($conn, $_POST['password_1']);
        $password_2 = mysqli_real_escape_string($conn, $_POST['password_2']);
        $tel = mysqli_real_escape_string($conn, $_POST['tel']);
        $fname = mysqli_real_escape_string($conn, $_POST['fname']);
        $lname = mysqli_real_escape_string($conn, $_POST['lname']);
        $address = mysqli_real_escape_string($conn, $_POST['address']);


        if($password_1 != $password_2){
            array_push($errors, "The two passwords do not match");
        }

        $user_check_query = "SELECT * FROM Rider WHERE username = '$username' OR email = '$email' OR nationalid = '$nationalid'";
        $query = mysqli_query($conn, $user_check_query);
        $result = mysqli_fetch_assoc($query);

        if($result){
            if($result['username'] === $username){
                array_push($errors, "Username already exists");
            }
            if($result['email'] === $email){
                array_push($errors, "Email already exists");
            }
            if($result['nationalid'] === $nationalid){
                array_push($errors, "National ID already exists");
            }
        }

        if(count($errors)==0){
            $password =md5($password_1);

            $sql = "INSERT INTO Rider (username, email, password, tel, firstname, lastname, nationalid, address) VALUES ('$username', '$email', '$password', '$tel', '$firstname', '$lastname', '$nationalid', '$address')";
            mysqli_query($conn,$sql);

            $_SESSION['username'] = $username;
            $_SESSION['success'] = "You are now logged in";
            header('location: rider_index.php');
        } else {
            array_push($errors, "Username or Email already exists");
            $_SESSION['error'] = "Username or Email already exists";
            header("location: rider_register.php");
        }
            
    }
?>