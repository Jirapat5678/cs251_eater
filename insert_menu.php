<?php
session_start();
include('server_shop.php');

$errors = array();

if(isset($_POST['add_menu'])){
    $dir = "picture/";
    $fname = mysqli_real_escape_string($conn, $_POST['fname']);
    $fprice= mysqli_real_escape_string($conn, $_POST['fprice']);
    $food_detail = mysqli_real_escape_string($conn, $_POST['fdetail']);
    $username = $_SESSION['username'];
    $fileImage = $dir . basename($_FILES["file"]["name"]);

    move_uploaded_file($_FILES['file']['tmp_name'], $fileImage);

    $mysql = "SELECT shopid FROM Shoper WHERE username='$username'";
    $query = mysqli_query($conn,$mysql);
    $result = mysqli_fetch_assoc($query);
    echo print_r($_SESSION['username']);
    
    $id = $result['shopid'];



    if(count($errors)==0){

        $sql = "INSERT INTO Menu (shopid,foodname, price, food_detail, food_pic) VALUES ('$id','$fname', '$fprice', '$food_detail','$fileImage')";
        mysqli_query($conn,$sql);

        $_SESSION['success'] = "เพิ่มเมนูสำเร็จ ขอบคุณค่ะ:)";
        header('location: shoper_index.php');
    } else {
        array_push($errors, "มีบางอย่างผิดพลาด โปรดตรวจสอบการนำเข้าข้อมูล");
        $_SESSION['error'] = "มีบางอย่างผิดพลาด โปรดตรวจสอบการนำเข้าข้อมูล";
        header("location: add_menu.php");
    }
        
}

if(isset($_POST['update'])){
    $dir = "picture/";
    $shopname= mysqli_real_escape_string($conn, $_POST['shopname']);
    $tel= mysqli_real_escape_string($conn, $_POST['tel']);
    $email = mysqli_real_escape_string($conn, $_POST['email']);
    $address = mysqli_real_escape_string($conn, $_POST['address']);
    $duty = mysqli_real_escape_string($conn, $_POST['duty']);
    $open = mysqli_real_escape_string($conn, $_POST['open']);
    $close = mysqli_real_escape_string($conn, $_POST['close']);

    $username= mysqli_real_escape_string($conn, $_POST['username']);
    $password= mysqli_real_escape_string($conn, $_POST['password']);
    $fileImage = $dir . basename($_FILES["file"]["name"]);

    move_uploaded_file($_FILES['file']['tmp_name'], $fileImage);

    $mysql = "SELECT shopid FROM Shoper WHERE username='$username'";
    $query = mysqli_query($conn,$mysql);
    $result = mysqli_fetch_assoc($query);
    

    if(count($errors)==0){

        $sql = "UPDATE shoper SET shopname='$shopname', tel='$tel',email='$email', address='$address',available='$duty',open='$open',close='$close',img_dir='$fileImage' WHERE username='$username' AND password='$password'";
        mysqli_query($conn,$sql);

        $_SESSION['success'] = "เพิ่มเมนูสำเร็จ ขอบคุณค่ะ:)";
        header('location: shoper_index.php');
    } else {
        array_push($errors, "มีบางอย่างผิดพลาด โปรดตรวจสอบการนำเข้าข้อมูล");
        $_SESSION['error'] = "มีบางอย่างผิดพลาด โปรดตรวจสอบการนำเข้าข้อมูล";
        header("location: add_menu.php");
    }
        
}

?>