<?php 
    session_start();
    include('server.php');
    
    if(!isset($_SESSION['username'])){
        $alert = '<script type="text/javascript">';
        $alert .= 'alert("ต้อง login ก่อน!");';
        $alert .= 'window.location.href ="login.php";';
        $alert .= '</script>';
      echo $alert;
    }

    if(isset($_SESSION['address'])){
        $alert = '<script type="text/javascript">';
        $alert .= 'alert("ต้องมี Adderssก่อน");';
        $alert .= 'window.location.href ="login.php";';
        $alert .= '</script>';
        echo $alert;
    }

    if(isset($_GET['logout'])){
        $alert = '<script type="text/javascript">';
        $alert .= 'alert("ขอบคุณที่ใช้บริการค่ะ");';
        session_destroy();
        unset($_SESSION['username']);
        $alert .= 'window.location.href ="login.php";';
        $alert .= '</script>';
        echo $alert;
    }

    $username = $_SESSION['username'];

    $link = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $str = explode("?",$link);
    $id = $str[1];
    $fid = $str[2];

    $mysql = "SELECT * FROM member WHERE username='$username'";
    $query_user = mysqli_query($conn,$mysql);
    $user= mysqli_fetch_assoc($query_user);

    $mysql = "SELECT * FROM shoper WHERE shopid='$id'";
    $query_shop = mysqli_query($conn,$mysql);
    $result = mysqli_fetch_assoc($query_shop);

    $mysql = "SELECT * FROM Menu WHERE shopid='$id' AND foodid='$fid'";
    $query_menu = mysqli_query($conn,$mysql);
    $menu = mysqli_fetch_assoc($query_menu);

    $mysql = "SELECT * FROM Cart ";
    $query_cart = mysqli_query($conn,$mysql);
    $cart = mysqli_fetch_assoc($query_cart);

    //$shopid = $result['shopid'];

    //$mysql = "SELECT * FROM Menu WHERE shopid = '$shopid'";
    //$query_show = mysqli_query($conn,$mysql);

    //debug
    //foreach($query_user as $data):
    //    echo '<pre>';
    //    print_r($data);
    //    echo '</pre>';
   // endforeach;

   // foreach($query_show as $data):
    //echo '<pre>';
    //print_r($data);
    //echo '</pre>';
   // endforeach;

    //debug
    //echo '<pre>';
    //print($id);
    //print_r($query_menu);
    //echo '</pre>';

 ?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<script type="text/javascript" src="https://api.longdo.com/map/?key=93a8dc22b5f210686e3140ad2ce40482"></script>
    <title>Eater</title>
    
</head>


<!--
<script>

function init() {
            map = new longdo.Map({
              placeholder: document.getElementById('map_canvas')
            });
          }

          
</script>
        -->
<style>
     button{
        border-radius: 0.5em;
        font-size:13px;
     }
     body{
        font-family: Tahoma, sans-serif;
        background: #C5C5C5;
     }
     .bar{
        width:100%;
        height:9%;
        position:absolute;
        background-color: #DC143C;
        display: inline-block;
     }
     .dropbtn{
        background: url('picture/menu.png');
        cursor: pointer;
        margin-left:50px;
        margin-top:20px;
        padding: 25px;
        border:none;
     }
     .dropdown {
        position: relative;
        display: inline-block;
    }
    .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f1f1f1;
        min-width: 160px;
    }
    .dropdown-content a {
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
    }
    .dropdown:hover .dropdown-content {display: block;}

    .dropdown-content a:hover {background-color: #ddd;}

    .search-bar{
        margin-left:500px;
        margin-top:-60px;
    }
    .search{
        background: url('picture/search.png');
        margin-top:10px;
        margin-left:10px;
        border:none;
        height: 20px;
        width: 20px;
    }
     .picture-group{
         border-radius: 25m; 
         margin-left: 170px;
     }
     .tab{
        width:39%;
        height:0.25%;
        position:absolute;
        background-color: #000000;
     }
     .detail-group{
         width:750px;
         height:auto;
         border-radius: 2em;
         background: #FFFFFF; 
         margin-left: 170px;
     }
     .menu-group{
         width:auto;
         height: 732px;
         border-radius: 2em;
         background: #FFFFFF; 
         right:0;
     }
     .group-image{
         border-radius: 2em;
         background: #CDCDCD;
         width: 200px;
         height: 150px;
         text-align: center;
         padding: 10px;
     }
     a:link, a:visited {
        color: #000000;
        text-decoration: underline;
        cursor: pointer;
    }
    .avatar{
        border-color: #000000;
        border-radius: 2em;
    }
    .frame {
        width: 300px;
        height: 250px;
        border: 3px solid #ccc;
        background: #eee;
        margin: auto;
    }


    
</style>
<body>
    <div class="header">

    </div>
    <!--
    <div id="map_canvas" style="width:80%;height:100vh"></div>
		</div>
-->
    
            <!-- บาร์ข้างบน -->
            <?php include("includes/bar.php"); ?>
            <br><br><br><br>
        
            <!-- content -->

            <form action = "order.php" method="post" enctype="/form-data">
            <div class="container p-3 bg-white fixed-center">
                <h1 class="ml-5">ร้าน<?php echo $result['shopname'] ?></h1>
                <div class="row">
                    <div class="col">
                        <img src="<?php echo $menu['food_pic'] ?>" class="p-3 rounded" style="max-width: 50%;margin-left:200px;"> 
                    </div>
                    
                        <div class="col">
                        <div class="card p-3 mx-auto text-center inline border border-secondary" style="max-width: 25rem;margin-top:60px;">
                            <h5> <input name="fname" value="<?=$menu['foodname'];?>" readonly class="justify-content-center text-center" style="background:#ffffff;border:0;"></input> </h5>
                            <h6> ราคาต่อหน่วย <input name="fprice" type="int" value="<?=$menu['price'];?>" readonly style="background:#ffffff;border:0;width:35px"></input>฿ </h6>
                                                       
                            <h7 class="card-body inline">จำนวนสินค้า</h7>

                            <input type="number" name="amount" min="1" value="1"class="mx-auto text-canter" style="border-radius: 0.5em;border-width: 0; box-shadow: 0 0 0 0.7pt #000000;height: 2em;width: 60px;font-size: 16px;">
                            <input name="foodid" value="<?=$menu['foodid'];?>" readonly style="background:#ffffff;;border:0;width:0;"></input>
                            <input name="shopid" value="<?=$menu['shopid'];?>" readonly style="background:#ffffff;border:0;width:0;"></input>
                            <input name="userid" value="<?=$user['id'];?>" readonly style="background:#ffffff;border:0;width:0;"></input>
                        </div><br><br><br><br><br>
                             
                        <div class="d-grid">
                        <button type="submit" class="btn btn-success rounded" name="order" >เพิ่มสินค้าลงในตะกร้า</button>
                        
                        </div>
                        </form>

                    </div>
    

                </div>
        
            
</body>
</html>