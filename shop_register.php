<?php
session_start(); 
include('server.php'); 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Eater</title>
</head>
<style>
    	.button{
  background-color: #C4C4C4;
  padding: 10px 20px;
  text-align: center;
  display: inline-block;
  font-size: 15px;
  cursor: pointer;
  border-radius: 3em;
      }
     input{
        border-radius: 0.5em;
        border-width: 0;
        box-shadow: 0 0 0 0.7pt #000000;
        height: 2em;
        width: 20em;
        font-size: 16px;
     }
     input.image{

     }
     body{
        font-family: Tahoma, sans-serif;
     }
     .input-group{
         width: 800px;
         padding: 50px;
         border-radius: 2em;
         position: absolute;
         top: 22%;
         left: 27.5%;
         background: #E6E5E5;
     }
     .header{
        width:100%;
        height:9%;
        position:absolute;
        top:0;
        left:0;
        right:0;
        background-color: #DC143C;
     }
     
</style>
<body>
    <div class="header">
        <a href="login.php">
    <img src="picture\logo.png" alt="logo" width="100" style="margin-left:30px;">
    </a>
    <a style="margin-left:1650px;font-size:20px;cursor: pointer;color:white;" title="99 หมู่ 18 อาคารสำนักงานทะเบียนนักศึกษา มหาวิทยาลัยธรรมศาสตร์ ศูนย์รังสิต
ตำบลคลองหนึ่ง อำเภอคลองหลวง จังหวัดปทุมธานี  12121">Contact us</a>
    </div>

    <form action = "register_db.php" method="post" enctype="multipart/form-data">
        <div class ="input-group">
            <h1 >Welcome our new</h1><br>
            <label>Username</label>
            <label style="margin-left:360px;">Shop's name</label><br>
            <input type="text" name="username" placeholder=" Enter your username" required>       
            <input type="text" name="shopname" placeholder=" Enter your shop name" style="margin-left: 100px"required><br><br>

            <label>E-mail</label>
            <label style="margin-left:385px;">วันและเวลาที่จะทำการเปิดร้าน</label><br>
            <input type="email" name="email" placeholder=" Enter your e-mail"required>
            <select style="border-radius: 2em;width: 120px;height:30px;margin-left: 100px" name="available">
                <option value="everyday">Everyday</option>
                <option value="weekday">Weekday</option>
                <option value="weekend">Weekend</option>
            </select><br><br>

            <label>Password</label>
            <label style="margin-left:365px;">เวลาเปิดร้าน</label><br>
            <input type="password" name="password_1" placeholder=" Enter your password"required>
            <input type="time" name="open" style="margin-left: 100px"required><br><br>

            <label>Confirm password</label>
            <label style="margin-left:300px;">เวลาปิดร้าน</label><br>
            <input type="password" name="password_2" placeholder=" Confirm your password" required>
            <input type="time" name="close" style="margin-left: 100px" required><br><br>

            <label >Phone number</label>
            <label style="margin-left:325px;">รูปร้านค้า</label>
            
            <input type="tel" name="tel" placeholder=" Enter your phone number" pattern="[0-9]{10}" required>
            <input type="file" name="file" style="margin-left: 100px" required></input><br><br>
            <label >ที่อยู่ของร้าน</label><br>
            <input type="text" name="address" placeholder=" Enter your shop's address" required></input><br><br>
            
            <a href="login_shoper.php" style="margin-left: 300px;">Already have an account?</a><br><br>
            <button type="submit" name="reg_shop" class="button" style="margin-left: 335px;">Register</button>
        </div>
       
    </form>

</body>
</html>