<?php
session_start(); 
include('server.php'); 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Eater</title>
</head>
<style>
    	.button{
  background-color: #C4C4C4;
  padding: 10px 20px;
  text-align: left;
  display: inline-block;
  font-size: 15px;
  cursor: pointer;
  border-radius: 3em;
      }
     input{
        border-radius: 0.5em;
        border-width: 0;
        box-shadow: 0 0 0 0.7pt #000000;
        height: 2em;
        width: 20em;
        font-size: 16px;
     }
     body{
        font-family: Tahoma, sans-serif;
     }
     .input-group{
         width: 800px;
         padding: 50px;
         border-radius: 2em;
         position: absolute;
         top: 22%;
         left: 27.7%;
         background: #E6E5E5;

     }
     .header{
        width:100%;
        height:9%;
        position:absolute;
        top:0;
        left:0;
        right:0;
        background-color: #DC143C;
     }
     
</style>
<body>
    <div class="header">
        <a href="login.php">
    <img src="picture\logo.png" alt="logo" width="100" style="margin-left:30px;">
    </a>
    <a style="margin-left:1650px;font-size:20px;cursor: pointer;color:white;" title="99 หมู่ 18 อาคารสำนักงานทะเบียนนักศึกษา มหาวิทยาลัยธรรมศาสตร์ ศูนย์รังสิต
ตำบลคลองหนึ่ง อำเภอคลองหลวง จังหวัดปทุมธานี  12121">Contact us</a>
    </div>

    <form action = "register_db.php" method="post">
        <div class ="input-group">
            <h1 style="margin-left:320px;">Sign up</h1><br>
            <label style="margin-right:250px;margin-bottom:25px;">Username</label>
            <label style="margin-left: 100px">First name</label><br>
            <input type="text" name="username" placeholder=" Enter your username" required>
            <input type="text" name="fname" placeholder=" Enter your first name" style="margin-left: 100px" required ><br><br>

            <label style="margin-right:270px;">E-mail</label>
            <label style="margin-left: 110px">Last name</label><br>
            <input type="email" name="email" placeholder=" Enter your e-mail"required>
            <input type="text" name="lname" placeholder=" Enter your last name" style="margin-left: 100px" required><br><br>
    
            <label style="margin-right:255px;">Password</label>
            <label style="margin-left: 105px">Address</label><br>
            <input type="password" name="password_1" placeholder=" Enter your password" required>
            <input type="text" name="address" placeholder=" Enter your address" style="margin-left: 100px" required><br><br>
            

            <label style="margin-right:200px;">Confirm password</label>
            <label style="margin-left: 105px">Gender</label><br>
            <input type="password" name="password_2" placeholder=" Confirm your password" required>
            <select name="gen" id="gen" style="margin-left:100px;"required>
                <option value="Male">Male</option>
                <option value="Female">Female</option>
                <option value="LGBTQ">LGBTQ+</option>
            </select><br><br>
            

            <label style="margin-right:215px;">Phone number</label><br>
            <input type="tel" name="tel" placeholder=" Enter your phone number" pattern="[0-9]{10}" required><br><br>


            <a href="login.php" style="margin-left: 290px">Already have an account?</a><br><br>
            <button type="submit" name="reg_user" class="button" style="margin-left: 330px">Register</button>
        </div>
    </form>

</body>
</html>