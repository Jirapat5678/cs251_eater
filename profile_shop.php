<?php 
    session_start();
    include('server_shop.php');
    
    if(!isset($_SESSION['username'])){
        $_SESSION['msg'] = "You must log in first";
        header("location; login.php");
    }

    if(isset($_SESSION['address'])){
        $alert = '<script type="text/javascript">';
        $alert .= 'alert("ต้องมี Adderssก่อน");';
        $alert .= 'window.location.href ="login.php";';
        $alert .= '</script>';
        echo $alert;
    }

    if(isset($_GET['logout'])){
        session_destroy();
        unset($_SESSION['username']);
        header('location; login.php');
    }


    $username = $_SESSION['username'];

    $mysql = "SELECT * FROM Shoper WHERE username='$username'";
    $query_shop = mysqli_query($conn,$mysql);
    $shop = mysqli_fetch_assoc($query_shop);

    $mysql = "SELECT * FROM Member WHERE username = '$username'";
    $query_user = mysqli_query($conn,$mysql);
    $user = mysqli_fetch_assoc($query_user);

    //debug
    //echo '<pre>';
    //print_r($result);
    //echo '</pre>';

 ?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<script type="text/javascript" src="https://api.longdo.com/map/?key=93a8dc22b5f210686e3140ad2ce40482"></script>
    <title>Eater</title>
    
</head>
<!--
<script>

function init() {
            map = new longdo.Map({
              placeholder: document.getElementById('map_canvas')
            });
          }

          
</script>
        -->
        <script>
            function previewImage(){
                var file = document.getElementById("file").files;
                if(file.length>0){
                    var fileReader = new FileReader();

                    fileReader.onload = function(event){
                        document.getElementById("preview").setAttribute("src",event.target.result);
                    };
                    fileReader.readAsDataURL(file[0]);
                }
            }
            </script>
<style>
     input{
        border-radius: 0.5em;
        border-width: 0;
        box-shadow: 0 0 0 0.7pt #000000;
        height: 2em;
        width: 20em;
        font-size: 16px;
     }
     button{
        border-radius: 0.5em;
        font-size:13px;
     }
     body{
        font-family: Tahoma, sans-serif;
        background: #C5C5C5;
     }
     .bar{
        width:100%;
        height:9%;
        position:absolute;
        background-color: #DC143C;
        display: inline-block;
     }
     .dropbtn{
        background: url('picture/menu.png');
        cursor: pointer;
        margin-left:50px;
        margin-top:20px;
        padding: 25px;
        border:none;
     }
     .dropdown {
        position: relative;
        display: inline-block;
    }
    .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f1f1f1;
        min-width: 160px;
    }
    .dropdown-content a {
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
    }
    .dropdown:hover .dropdown-content {display: block;}

    .dropdown-content a:hover {background-color: #ddd;}

    .search-bar{
        margin-left:500px;
        margin-top:-60px;
    }
    .search{
        background: url('picture/search.png');
        margin-top:10px;
        margin-left:10px;
        border:none;
        height: 20px;
        width: 20px;
    }
     .picture-group{
         border-radius: 25m; 
         margin-left: 170px;
     }
     .tab{
        width:39%;
        height:0.25%;
        position:absolute;
        background-color: #000000;
     }
     .detail-group{
         width:750px;
         height:auto;
         border-radius: 2em;
         background: #FFFFFF; 
         margin-left: 170px;
     }
     .menu-group{
         width:auto;
         height: 732px;
         border-radius: 2em;
         background: #FFFFFF; 
         right:0;
     }
     .group-image{
         border-radius: 2em;
         background: #CDCDCD;
         width: 200px;
         height: 150px;
         text-align: center;
         padding: 10px;
     }
     a:link, a:visited {
        color: #000000;
        text-decoration: underline;
        cursor: pointer;
    }
    .avatar{
        border-color: #000000;
        border-radius: 2em;
    }
    
</style>
<body>
    <div class="header">

    </div>
    <!--
    <div id="map_canvas" style="width:80%;height:100vh"></div>
		</div>
-->
    
            <!-- บาร์ข้างบน -->
            <div class="bar">
        <a href="shoper_index.php">
        <img src="picture\logo.png" alt="logo" width="100" style="margin-left:30px;float:left;">
        </a>
        
            <!-- MENU -->
            <div class="dropdown">
                <button class="dropbtn"></button>
                <div class="dropdown-content">
                    <a href="shoper_index.php">My shop</a>
                    <a href="#">My order</a>
                    <a href="#">Wallet</a>
                    <a href="#">Report</a>
                    <a href="login.php">Logout</a>
                </div>
            </div>

        </div>
            
        <!-- FOOD-MENU -->
        <br><br><br><br><br>

     <br>
     
    <!-- IMAGE -->


    <div class="container bg-white position-abosolute rounded p-3"  style="max-width: 80rem;">

        <div class="row d-flex" >
            <div class="col" >
    <div class="app-card app-card-settings shadow-sm p-4 bg-white rounded">
                <div class="app-card-body">
            <form action="insert_menu.php" method="post" enctype="multipart/form-data">   
                <label class="form-label">รูปภาพร้านค้า</label>
                <div class="mb-3">
                <img id="preview" class="rounded" width="350" height="350" >
            </div>
        <input type="file" name="file" id="file" accept="image/*" value="<?php echo $shop['img_dir']; ?>" onchange="previewImage();">
            </div>
            </div>
            </div>

            <div class="col mt-5 mx-auto">
            <label for="username" class="mt-2 mx-5">Username :</label><br><br>
            <label for="shopname" class="mt-2 mx-5">Shop name :</label><br><br>
            <label for="tel" class="mt-2 mx-5" >Phone number :</label><br><br>
            <label for="email" class="mt-2 mx-5" >E-mail :</label><br><br>
            <label for="address" class="mt-2 mx-5" >Address :</label><br><br>
            <label for="duty" class="mt-2 mx-5" >Day on duty :</label><br><br>
            <label for="open" class="mt-2 mx-5" >Open :</label><br><br>
            <label for="close" class="mt-2 mx-5" >Close :</label><br><br>
            </div>
                <div class="col mt-5">
            <input type="text" name="username" value="<?php echo $shop['username']; ?>" disabled><br><br>
            <input type="text" name="shopname" value="<?php echo $shop['shopname']; ?>" ><br><br>
            <input type="text" name="tel" value="<?php echo $shop['tel']; ?>"><br><br>
            <input type="email" name="email" value="<?php echo $shop['email']; ?>"><br><br>
            <input type="text" name="address" value="<?php echo $shop['address']; ?>"><br><br>
            <select style="border-radius: 2em;width: 120px;height:30px;margin-left: 100px" name="duty">
                <option value="Everyday">Everyday</option>
                <option value="Weekday">Weekday</option>
                <option value="Weekend">Weekend</option>
            </select><br><br>
            <input type="time" name="open" value="<?php echo $shop['open']; ?>"><br><br>
            <input type="time" name="close" value="<?php echo $shop['close']; ?>"><br><br>
            <input type="hidden" name="password" value="<?php echo $shop['password']; ?>">
            <input type="hidden" name="username" value="<?php echo $shop['username']; ?>">
</div>
        </div> <!-- row -->

            <br><br>
                <button type="submit" name="update" class="position-relative top-50 start-50 translate-middle btn btn-success center"> ยืนยันการเปลี่ยนแปลงข้อมูล</button>

              
    </div> <!-- container -->
  
                   
</form>
          
</body>
</html>