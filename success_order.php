<?php 
    session_start();
    include('server_shop.php');
    
    if(!isset($_SESSION['username'])){
        $alert = '<script type="text/javascript">';
        $alert .= 'alert("ต้อง login ก่อน!");';
        $alert .= 'window.location.href ="login.php";';
        $alert .= '</script>';
      echo $alert;
    }

    if(isset($_SESSION['address'])){
        $alert = '<script type="text/javascript">';
        $alert .= 'alert("ต้องมี Adderssก่อน");';
        $alert .= 'window.location.href ="login.php";';
        $alert .= '</script>';
        echo $alert;
    }

    if(isset($_GET['logout'])){
        $alert = '<script type="text/javascript">';
        $alert .= 'alert("ขอบคุณที่ใช้บริการค่ะ");';
        session_destroy();
        unset($_SESSION['username']);
        $alert .= 'window.location.href ="login.php";';
        $alert .= '</script>';
        echo $alert;
    }
    $all=0;
    

    $username = $_SESSION['username'];

    $mysql = "SELECT * FROM Member WHERE username = '$username'";
    $query_user = mysqli_query($conn,$mysql);
    $user = mysqli_fetch_assoc($query_user);

    $link = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $str = explode("?",$link);
    $id = $str[1];
    $cart = $str[2];

    $sql = "SELECT count(amount) as total FROM cart WHERE owner='$id'";
    $query_amount = mysqli_query($conn,$sql);
    $amount = mysqli_fetch_assoc($query_amount);

    $sql = "SELECT sum(total) as price FROM cart WHERE owner='$id'";
    $query_amount = mysqli_query($conn,$sql);
    $all = mysqli_fetch_assoc($query_amount);

    $mysql = "SELECT * FROM cart WHERE owner='$id'";
    $query_user = mysqli_query($conn,$mysql);
    $list_order = mysqli_fetch_assoc($query_user);

    $shopid = $list_order['shopid'];

    $mysql = "SELECT * FROM shoper WHERE shopid='$shopid'";
    $query_shop = mysqli_query($conn,$mysql);
    $result = mysqli_fetch_assoc($query_shop);

    $mysql = "SELECT * FROM Menu WHERE shopid='$id'";
    $query_menu = mysqli_query($conn,$mysql);

    $sql = "SELECT * FROM invoice WHERE userid='$id' AND cartid='$cart'";
    $query_status = mysqli_query($conn,$sql);
    $status = mysqli_fetch_assoc($query_status);

    $mysql = "SELECT * FROM invoice WHERE status ='กำลังไปยังลูกค้า'";
    $query = mysqli_query($conn,$mysql);
    $received_order = mysqli_fetch_assoc($query);

    $pri_1='secondary';
    $pri_2='secondary';
    $pri_3='secondary';

    if($status['status'] == 'กำลังหาไรเดอร์'){
        $pri_1 = "primary";
        $bar = "0";
    }else if($status['status'] == 'กำลังทำอาหาร'){
        $pri_1 = "primary";
        $pri_2 = "primary";
        $bar = "50";
    }else if($status['status'] == 'กำลังไปยังลูกค้า'){
        $pri_1 = "primary";
        $pri_2 = "primary";
        $pri_3 = "primary";
        $bar = "100";
    }

    

    //$shopid = $result['shopid'];

    //$mysql = "SELECT * FROM Menu WHERE shopid = '$shopid'";
    //$query_show = mysqli_query($conn,$mysql);

    //debug
    //foreach($query_user as $data):
    //    echo '<pre>';
    //    print_r($data);
    //    echo '</pre>';
   // endforeach;

   // foreach($query_show as $data):
    //echo '<pre>';
    //print_r($data);
    //echo '</pre>';
   // endforeach;

    //debug
    //echo '<pre>';
    //print($id);
    //print_r($result);
    //echo '</pre>';

 ?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<script type="text/javascript" src="https://api.longdo.com/map/?key=93a8dc22b5f210686e3140ad2ce40482"></script>
    <title>Eater</title>
    
</head>

<!--
<script>

function init() {
            map = new longdo.Map({
              placeholder: document.getElementById('map_canvas')
            });
          }

          
</script>
        -->
<style>
     input{
        border-radius: 0.5em;
        border-width: 0;
        height: 2em;
        width: 20em;
        font-size: 16px;
     }
     button{
        border-radius: 0.5em;
        font-size:13px;
     }
     body{
        font-family: Tahoma, sans-serif;
        background: #C5C5C5;
     }
     .bar{
        width:100%;
        height:9%;
        position:absolute;
        background-color: #DC143C;
        display: inline-block;
     }
     .dropbtn{
        background: url('picture/menu.png');
        cursor: pointer;
        margin-left:50px;
        margin-top:20px;
        padding: 25px;
        border:none;
     }
     .dropdown {
        position: relative;
        display: inline-block;
    }
    .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f1f1f1;
        min-width: 160px;
    }
    .dropdown-content a {
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
    }
    .dropdown:hover .dropdown-content {display: block;}

    .dropdown-content a:hover {background-color: #ddd;}

    .search-bar{
        margin-left:500px;
        margin-top:-60px;
    }
    .search{
        background: url('picture/search.png');
        margin-top:10px;
        margin-left:10px;
        border:none;
        height: 20px;
        width: 20px;
    }
     .picture-group{
         border-radius: 25m; 
         margin-left: 170px;
     }
     .tab{
        width:39%;
        height:0.25%;
        position:absolute;
        background-color: #000000;
     }
     .detail-group{
         width:750px;
         height:auto;
         border-radius: 2em;
         background: #FFFFFF; 
         margin-left: 170px;
     }
     .menu-group{
         width:auto;
         height: 732px;
         border-radius: 2em;
         background: #FFFFFF; 
         right:0;
     }
     .group-image{
         border-radius: 2em;
         background: #CDCDCD;
         width: 200px;
         height: 150px;
         text-align: center;
         padding: 10px;
     }
     a:link, a:visited {
        color: #000000;
        text-decoration: underline;
        cursor: pointer;
    }
    .avatar{
        border-color: #000000;
        border-radius: 2em;
    }
    .frame {
        width: 300px;
        height: 250px;
        border: 3px solid #ccc;
        background: #eee;
        margin: auto;
    }


    
</style>
<body>
    <div class="header">

    </div>
    <!--
    <div id="map_canvas" style="width:80%;height:100vh"></div>
		</div>
-->
    
            <!-- บาร์ข้างบน -->
            <?php include("includes/bar.php"); ?>
            <br><br><br><br>
        
            <!-- content -->

            <form method="post" enctype="/form-data" action="order.php">
            <div class="container p-3 bg-white fixed-center">
                <h1>สถานะการจัดส่ง</h1>
                <hr>
                
                <div class="card">
                    <div class="row">
                    <div class="col-12">
                    <div class="position-relative m-4">
                    <div class="progress" style="height: 2px;">
                    <div class="progress-bar" role="progressbar" style="width: <?php echo $bar; ?>%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <button type="button" class="position-absolute top-0 start-0 translate-middle btn btn-sm btn-<?php echo $pri_1;?> rounded-pill" style="width: 2rem; height:2rem;">1</button>
                    <button type="button" class="position-absolute top-0 start-50 translate-middle btn btn-sm btn-<?php echo $pri_2;?> rounded-pill" style="width: 2rem; height:2rem;">2</button>
                    <button type="button" class="position-absolute top-0 start-100 translate-middle btn btn-sm btn-<?php echo $pri_3;?> rounded-pill" style="width: 2rem; height:2rem;">3</button>
                    </div>
                    </div>
                     </div>
                    </div> <!-- card -->

                <hr>


                <div class="row">
                        
                    <div class="col mx-5 mt-5">

                        <div class="container mt-5 border border-secondary rounded w-50">
                        <h3 class="fs-5">สถานะ: <?=$status['status'];?></h3>
                        </div>
                    </div>

                    

                    <div class="col mt-5 text-center">
                    <h1 class="fs-5">รายการอาหารที่สั่งทั้งหมด<?=$amount['total'];?>รายการ</h1>

                    <div class="container mt-3 p-3 border border-secondary rounded w-50 ">
                    <h1 class="fs-5">ยอดชำระ <?=$all['price'] ?> ฿</h1>
                    <h1 class="text-success fs-4">ชำระเงินเรียบร้อย</h1>
                    </div>

                    </div>

                </div> <!-- row -->

                <input type="hidden" id="orderid" name="orderid" value="<?=$received_order['orderid'];?>">
                <input type="hidden" id="owner" name="owner" value="<?=$list_order['owner'];?>">
                <br>
                <button class="btn btn-success" style="margin-left:590px;"name="accept">ยืนยันสินค้า</button>
                </div> <!-- container -->
                </form>
</body>
</html>