<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<script type="text/javascript" src="https://api.longdo.com/map/?key=93a8dc22b5f210686e3140ad2ce40482"></script>
    <title>Eater</title>
    
</head>
<style>
     input{
        border-radius: 0.5em;
        border-width: 0;
        box-shadow: 0 0 0 0.7pt #000000;
        height: 2em;
        width: 20em;
        font-size: 16px;
     }
     body{
        font-family: Tahoma, sans-serif;
        background: linear-gradient(
          rgba(0, 0, 0, 0.4), 
          rgba(0, 0, 0, 0.4)
        ),
        url('picture/BG.jpg');
     }
     .input-group{
         width: 350px;
         padding: 50px;
         border-radius: 2em;
         position: absolute;
         top: 25%;
         left: 39%;
         background: #E6E5E5;
         text-align: center;
     }
     .bar{
        width:100%;
        height:9%;
        position:absolute;
        background-color: #DC143C;
        display: inline-block;
     }
     .tab{
        width:76.50%;
        height:0.25%;
        position:absolute;
        background-color: #000000;
     }
     .dropbtn{
        background: url('picture/menu.png');
        cursor: pointer;
        margin-left:50px;
        margin-top:20px;
        padding: 25px;
        border:none;
     }
     .dropdown {
        position: relative;
        display: inline-block;
    }
    .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f1f1f1;
        min-width: 160px;
    }
    .dropdown-content a {
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
    }
    .dropdown:hover .dropdown-content {display: block;}

    .dropdown-content a:hover {background-color: #ddd;}

    .search-bar{
        margin-left:500px;
        margin-top:-60px;
    }
    .search{
        background: url('picture/search.png');
        margin-top:10px;
        margin-left:10px;
        border:none;
        height: 20px;
        width: 20px;
    }
    .suggest{
      padding-left: 90px;
      font-size: 26px;
     }
     .group-suggest{
         width: 1350px;
         height: 500px;
         border-radius: 2em;
         background: #FFFFFF; 
         padding: 10px;
     }
     .group-promotion{
         width: 1350px;
         height: 300px;
         border-radius: 2em;
         background: #FFFFFF; 
         padding: 10px;
     }
     .group-total{
         width: 1350px;
         height: auto;
         border-radius: 2em;
         background: #FFFFFF; 
         padding: 10px;
         margin-left: 210px;
     }
     .image{
      padding-left: 90px;
      margin-left: 50px;
      letter-spacing: 64px;

     }
     .group-image{
         border-radius: 2em;
         background: #CDCDCD;
         width: 200px;
         height: 150px;
         text-align: center;
         padding: 10px;
     }
     .name{
      font-size: 22px;
      padding-left: 160px;
      
     }
     .group-name{
      width: 100px;
         height: 30px;
         border-radius: 2em;
         background: #FFFFFF; 
         padding: 10px;
         margin-left: 75px;
     }
     a:link, a:visited {
        color: #000000;
        text-decoration: underline;
        cursor: pointer;
    }
</style>