<?php 
    session_start();
    include('server_shop.php');
    
    if(!isset($_SESSION['username'])){
        $alert = '<script type="text/javascript">';
        $alert .= 'alert("ต้อง login ก่อน!");';
        $alert .= 'window.location.href ="login.php";';
        $alert .= '</script>';
      echo $alert;
    }

    if(isset($_SESSION['address'])){
        $alert = '<script type="text/javascript">';
        $alert .= 'alert("ต้องมี Adderssก่อน");';
        $alert .= 'window.location.href ="login.php";';
        $alert .= '</script>';
        echo $alert;
    }

    if(isset($_GET['logout'])){
        $alert = '<script type="text/javascript">';
        $alert .= 'alert("ขอบคุณที่ใช้บริการค่ะ");';
        session_destroy();
        unset($_SESSION['username']);
        $alert .= 'window.location.href ="login.php";';
        $alert .= '</script>';
        echo $alert;
    }

    $username = $_SESSION['username'];

    $mysql = "SELECT * FROM shoper WHERE username = '$username'";
    $query_user = mysqli_query($conn,$mysql);
    $result = mysqli_fetch_assoc($query_user);

    $shopid = $result['shopid'];

    $mysql = "SELECT * FROM Menu WHERE shopid = '$shopid'";
    $query_show = mysqli_query($conn,$mysql);

    //debug
    //foreach($query_user as $data):
    //    echo '<pre>';
    //    print_r($data);
    //    echo '</pre>';
   // endforeach;

   // foreach($query_show as $data):
    //echo '<pre>';
    //print_r($data);
    //echo '</pre>';
   // endforeach;

    //debug
    //echo '<pre>';
    //print_r($result);
    //echo '</pre>';

 ?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<script type="text/javascript" src="https://api.longdo.com/map/?key=93a8dc22b5f210686e3140ad2ce40482"></script>
    <title>Eater</title>
    
</head>
<!--
<script>

function init() {
            map = new longdo.Map({
              placeholder: document.getElementById('map_canvas')
            });
          }

          
</script>
        -->
<style>
     input{
        border-radius: 0.5em;
        border-width: 0;
        height: 2em;
        width: 20em;
        font-size: 16px;
     }
     button{
        border-radius: 0.5em;
        font-size:13px;
     }
     body{
        font-family: Tahoma, sans-serif;
        background: #C5C5C5;
     }
     .bar{
        width:100%;
        height:9%;
        position:absolute;
        background-color: #DC143C;
        display: inline-block;
     }
     .dropbtn{
        background: url('picture/menu.png');
        cursor: pointer;
        margin-left:50px;
        margin-top:20px;
        padding: 25px;
        border:none;
     }
     .dropdown {
        position: relative;
        display: inline-block;
    }
    .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f1f1f1;
        min-width: 160px;
    }
    .dropdown-content a {
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
    }
    .dropdown:hover .dropdown-content {display: block;}

    .dropdown-content a:hover {background-color: #ddd;}

    .search-bar{
        margin-left:500px;
        margin-top:-60px;
    }
    .search{
        background: url('picture/search.png');
        margin-top:10px;
        margin-left:10px;
        border:none;
        height: 20px;
        width: 20px;
    }
     .picture-group{
         border-radius: 25m; 
         margin-left: 170px;
     }
     .tab{
        width:39%;
        height:0.25%;
        position:absolute;
        background-color: #000000;
     }
     .detail-group{
         width:750px;
         height:auto;
         border-radius: 2em;
         background: #FFFFFF; 
         margin-left: 170px;
     }
     .menu-group{
         width:auto;
         height: 732px;
         border-radius: 2em;
         background: #FFFFFF; 
         right:0;
     }
     .group-image{
         border-radius: 2em;
         background: #CDCDCD;
         width: 200px;
         height: 150px;
         text-align: center;
         padding: 10px;
     }
     a:link, a:visited {
        color: #000000;
        text-decoration: underline;
        cursor: pointer;
    }
    .avatar{
        border-color: #000000;
        border-radius: 2em;
    }
    
</style>
<body>
    <div class="header">

    </div>
    <!--
    <div id="map_canvas" style="width:80%;height:100vh"></div>
		</div>
-->
    
            <!-- บาร์ข้างบน -->
            <div class="bar">
        <a href="shoper_index.php">
        <img src="picture\logo.png" alt="logo" width="100" style="margin-left:30px;float:left;">
        </a>
        
            <!-- MENU -->
            <div class="dropdown">
                <button class="dropbtn"></button>
                <div class="dropdown-content">
                    <a href="profile_shop.php?<?php echo $result['shopid']; ?>">Profile</a>
                    <a href="?logout='1'">Logout</a>
                </div>
            </div>
            
        </div>
            
        <!-- FOOD-MENU -->
        <br><br><br><br><br>
        
       <div class="float-start" style="margin-left:170px;">
             <br>
             <h1 >ร้าน<?php echo $result['shopname']; ?></h1><br>
             <img src="<?php echo $result['img_dir']; ?>" width="300" alt="Avatar" class="avatar">
             
         <br>

     </div>
     <br>
    <!-- MENU -->
       <div class="container-md p-3 mt-5 bg-white rounded position-relative border border-dark" style="max-width: 58rem;margin-left:970px;">
           <div class ="title">
                <div class="text-center">MENU</div>
                <hr><br>
                <div class="row">
                <div class="col-lg d-flex p-2">
           <?php foreach($query_show as $data):?>
            
                <div class="card p-3 mx-auto" style="max-width: 12rem;margin-top:-30px;">
                    <img src="<?php echo $data['food_pic']; ?>" class="card-img-top">
                    <div class="card-body">
                        <h5 class="card-title inline"><?=$data['foodname']?></h5>
                        <h6 class="card-title inline"><?=$data['price']?></h6>
         </div>
         </div>
         
        <?php endforeach; ?>
             </div>
           </div>
        </div>

        <br>

        <a style="margin-left:550px" href="add_menu.php">Add menu</a>
        </div>
     <!-- DETAIL -->
    <div class="container p-3 mt-5 bg-white rounded d-flex flex-column position-fixed border border-dark" style="max-width: 45rem;margin-left:170px;margin-top:50px">
         <h2 style="margin-left:20px;">ร้าน<?php echo $result['shopname']; ?></h2><br>
         <h2 style="margin-left:20px;font-size:25px;">เวลาเปิด :<?php echo $result['available']; ?> <?php echo $result['open']; ?> - <?php echo $result['close']; ?></h2>
         <h2 style="margin-left:20px;font-size:25px;">ที่อยู่ของร้าน :<?php echo $result['address']; ?></h2><br>
         <h2 style="margin-left:20px;font-size:25px;">เบอร์ติดต่อ :<?php echo $result['tel']; ?></h2><br>
         <a class="btn btn-secondary" style="margin-left:550px" href="profile_shop.php?<?php echo $result['shopid'];?>">Edit</a>
       </div>

       
            

            
</body>
</html>