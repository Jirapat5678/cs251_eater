<?php
session_start(); 
include('server.php'); 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Kanit">
	<link href="https://fonts.googleapis.com/css2?family=IBM+Plex+Sans+Thai+Looped:wght@300&display=swap" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <title>Eater</title>
</head>
<style>
    	.button{
  background-color: #C4C4C4;
  padding: 10px 20px;
  text-align: left;
  display: inline-block;
  font-size: 15px;
  cursor: pointer;
  border-radius: 3em;
      }
     input{
        border-radius: 0.5em;
        border-width: 0;
        box-shadow: 0 0 0 0.7pt #000000;
        height: 2em;
        width: 20em;
        font-size: 16px;
     }
     body{
        font-family: Tahoma, sans-serif;
     }
     .input-group{
         width: 350px;
         padding: 50px;
         border-radius: 2em;
         position: absolute;
         top: 25%;
         left: 39%;
         background: #E6E5E5;
         text-align: center;
     }
     .bar{
        width:100%;
        height:9%;
        position:absolute;
        top:0;
        left:0;
        right:0;
        background-color: #DC143C;
     } 
</style>
<body>

    <div class="bar">
    <a href="login.php">
        <img src="picture\logo.png" alt="logo" width="100" style="margin-left:30px;">
        
    </a>
        <a style="margin-left:1650px;font-size:20px;cursor: pointer;color:white;" title="99 หมู่ 18 อาคารสำนักงานทะเบียนนักศึกษา มหาวิทยาลัยธรรมศาสตร์ ศูนย์รังสิต
ตำบลคลองหนึ่ง อำเภอคลองหลวง จังหวัดปทุมธานี  12121">Contact us</a>
        
    </div>
            
     <!-- notification message-->
     <?php if (isset($_SESSION['error'])) : ?>
                    <div class="error">
                        <h3>
                            <?php 
                                echo $_SESSION['error'];
                                unset($_SESSION['error']);
                            ?>
                        </h3>
                    </div>
        <?php endif ?>

    <form action = "login_db.php" method="post">

        <div class="input-group">
            
        <img src="picture\logo.png" alt="logo" width="200" style="margin-bottom:30px;">
            <input type="text" name="username" placeholder=" Username" required><br><br>
            <input type="password" name="password" placeholder=" Password" required><br><br>
    
            <button type="submit" name="login_user" class="button" style="margin-right:60px">Sign in</button>
            <button type="button" class="button" onclick="location.href='register.php';">Create new account</button><br><br>
            <button type="button" class="button" onclick="location.href='login_shoper.php';">saler?</button>
            <button type="button" class="button" onclick="location.href='login_rider.php';">rider?</button>
    </form>

</body>
</html>